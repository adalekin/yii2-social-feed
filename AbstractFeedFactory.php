<?php
/**
 * Created by PhpStorm.
 * User: adalekin
 * Date: 28.07.15
 * Time: 15:06
 */

namespace yii\socialfeed;

/*
 * FeedFactory classes
 */
abstract class AbstractFeedFactory {
    abstract function getPosts($link, $limit = 200);
}
