<?php
/**
 * Created by PhpStorm.
 * User: adalekin
 * Date: 28.07.15
 * Time: 15:13
 *
 *
 */

namespace yii\socialfeed;

use MetzWeb\Instagram\Instagram;

class InstagramFeedPost extends AbstractFeedPost
{
    const INSTAGRAM_PAGE_URL = 'https://www.instagram.com/p/{code}/';

    public function __construct($raw, $author)
    {
        $this->author = $author;
        $this->link = str_replace('{code}', $raw->code, self::INSTAGRAM_PAGE_URL);

        $this->image = null;
        $this->video = null;
        $this->description = null;

        if ($raw->is_video === false) {
            $this->image = [
                "low_resolution" => null,
                "thumbnail" => $raw->thumbnail_src,
                "standard_resolution" => $raw->display_src
            ];
        } else {
            $this->video = $raw->video_url;
        }

        if (!empty($raw->caption)) {
            $this->description = $raw->caption;
        }

        $this->date = $raw->date;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getNetwork()
    {
        return "instagram";
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getVideo()
    {
        return $this->video;
    }
}

class InstagramFeedFactory extends AbstractFeedFactory
{
    private $baseLink;

    const INSTAGRAM_BASE_URL = 'https://www.instagram.com/';
    const INSTAGRAM_PAGE_URL = 'https://www.instagram.com/p/{code}/';
    const USERAGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36';

    public function __construct($params)
    {
        //$this->client_id = $params['client_id'];
        //$this->client_secret = $params['client_secret'];
        //$this->api = new Instagram($this->client_id);
    }

    /**
     * Crawl instagram posts
     *
     * @param type $link  base link to crawl
     * @param type $limit  limit results array
     * @return array of \yii\socialfeed\InstagramFeedPost
     */
    public function getPosts($link, $limit = 200)
    {
        $results = [];
        $maxId = null;
        do {
            $data = $this->makeCall($this->makeLink($link, ['__a' => 1, 'max_id' => $maxId]));
            $author = $this->getAuthor($data);
            $maxId = $data->user->media->page_info->end_cursor;
            foreach ($data->user->media->nodes as $post) {
                if ($this->isVideo($post)) {
                    $post->video_url = $this->getVideoUrl($post);
                }
                $results[] = new InstagramFeedPost($post, $author);
            }
            if (count($results) > $limit) {
                break;
            }
        } while ($this->hasNextPage($data));
        return $results;
    }

    /**
     * Get direct url to video
     *
     * @param InstagramPost $post
     * @return string
     */
    private function getVideoUrl($post)
    {
        $link = str_replace('{code}', $post->code, self::INSTAGRAM_PAGE_URL);
        $data = $this->makeCall($this->makeLink($link, ['__a' => 1]));
        return $data->media->video_url;
    }

    /**
     * @deprecated
     */
    public function getPostsOld($link, $limit = 200)
    {
        $this->baseLink = $link;
        if (preg_match("/instagram.com\/([A-z0-9_]+)/", $link, $match)) {
            $username = $match[1];
            $users = $this->api->searchUser($username, 20);
            foreach ($users->data as $u) {
                if ($u->username == $username) {
                    $user = $u;
                }
            }

            $posts = $this->api->getUserMedia($user->id);
            $result = [];

            do {
                foreach ($posts->data as $post) {
                    $result[] = new InstagramFeedPost($post);
                }

                // TODO: splice array to limit
                if (count($result) > $limit) {
                    break;
                }
            } while ($posts = $this->api->pagination($posts));

            return $result;
        }
        return [];
    }

    /**
     * Check if we have some more instagram posts
     *
     * @param Object $data JSON object of instagram page
     * @return boolean true if has next pages
     */
    private function hasNextPage($data)
    {
        return (boolean) $data->user->media->page_info->has_next_page;
    }

    /**
     * Check if we have some more instagram posts
     *
     * @param InstagramPost $post object of instagram post
     * @return boolean true if post has video
     */
    private function isVideo($post)
    {
        return (boolean) $post->is_video;
    }

    /**
     * Make author array
     *
     * @param Object $data JSON object of instagram page
     * @return array
     */
    private function getAuthor($data)
    {
        $result['id'] = $data->user->id;
        $result['name'] = $data->user->username;
        $result['image'] = $data->user->profile_pic_url;
        $result['link'] = self::INSTAGRAM_BASE_URL.$data->user->username;
        return $result;
    }

    /**
     * Make link to request instagram JSON objects
     *
     * @param string $link
     * @param array $params
     * @return string
     */
    private function makeLink($link, $params = [])
    {
        $query = http_build_query($params);
        return $link.'?'.$query;
    }

    /**
     * Call url via curl
     *
     * @param string $url
     * @return JSON Object
     * @throws \Exception
     */
    protected function makeCall($url)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($handle, CURLOPT_REFERER, self::INSTAGRAM_BASE_URL);
        curl_setopt($handle, CURLOPT_USERAGENT, self::USERAGENT);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, true);
        $jsonData = curl_exec($handle);
        if (false === $jsonData) {
            throw new \Exception("Error: makeCall() - cURL error: ".curl_error($handle));
        }
        curl_close($handle);

        return json_decode($jsonData);
    }
}
