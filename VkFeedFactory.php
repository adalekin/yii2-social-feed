<?php
/**
 * @author Driverok <driverok@gmail.com>
 */

namespace yii\socialfeed;

//use getjump\Vk\Core;
//use getjump\Vk\Auth;

/**
 * Vk Post
 */
class VkFeedPost extends AbstractFeedPost
{

    public function __construct($author, $date, $text, $photo, $video, $link)
    {
        $this->image = null;
        $this->video = null;
        $this->author = $author;
        if (!empty($photo)) {
            $this->image = $photo;
        }
        if (!empty($video)) {
            $this->video = $video;
        }

        $this->description = $text;
        $this->date = $date;
        $this->link = $link;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getNetwork()
    {
        return "vk";
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getVideo()
    {
        return $this->video;
    }
}

/**
 * Calling Vk api to get Posts from base wall url
 *
 * working only with public data
 */
class VkFeedFactory extends AbstractFeedFactory
{
    const URL_VK_API = 'https://api.vk.com/method/';
    const VK_API_VERSION = '5.57';
    const VK_API_MAX_POSTS = 100;
    const VK_API_IMAGE_LOW = 'low_resolution';
    const VK_API_IMAGE_MEDIUM = 'thumbnail';
    const VK_API_IMAGE_HIGH = 'standard_resolution';

    private $fieldMap = [
        'q' => self::VK_API_IMAGE_LOW,
        'y' => self::VK_API_IMAGE_MEDIUM,
        'w' => self::VK_API_IMAGE_HIGH,
    ];
    private $VKImageSizes = [
        'w',
        'z',
        'y',
        'q',
        'p',
        'o',
        'x',
        'm',
        's'
    ];
    private $socialFeedImageSizes = [
        self::VK_API_IMAGE_HIGH,
        self::VK_API_IMAGE_MEDIUM,
        self::VK_API_IMAGE_LOW
    ];
    private $guzzle;

    public function __construct()
    {
    }

    /**
     * Map vk image sizes to socialfeed image sizes
     *
     * @deprecated
     * @param string $type
     * @return string
     */
    private function imageTypeMap($type)
    {
        if (isset($this->fieldMap[$type])) {
            return $this->fieldMap[$type];
        }
        return $type;
    }

    /**
     * Make array of post images, using largest as self::VK_API_IMAGE_HIGH, and so on...
     *
     * @deprecated
     * @param array $sizesArr array of vk images with type as a key
     * @return array
     */
    private function replaceSizes($sizesArr)
    {
        $result = [];
        $feedImages = $this->socialFeedImageSizes;
        foreach ($this->VKImageSizes as $size) {
            if (empty($sizesArr[$size]) or empty($feedImages)) {
                continue;
            }
            $result[array_shift($feedImages)] = $sizesArr[$size];
        }
        return $result;
    }

    /**
     * Get photos of the post
     * @param VkPost $post
     * @return array array of photos of the post
     */
    private function getPhoto($post)
    {
        if (empty($post->attachments)) {
            return;
        }
        $results = [];
        $images = [];
        $hasPhotos=false;
        foreach ($post->attachments as $attachment) {
            if ($hasPhotos===true)
            {
                continue;
            }
            switch ($attachment->type) {
                case 'photo':
                    $photos = $this->getPhotoImages($attachment);
                    $images[] = $photos;
                    if (!empty($photos))
                    {
                        $hasPhotos=true;
                    }
                    break;
                case 'album':
                    $images[] = $this->getAlbumImages($attachment);

                    break;
                case 'video':
                    $images[] = $this->getVideoImages($attachment);
                    break;
                case 'link':
                    $images[] = $this->getLinkImages($attachment);
                    break;
            }
            if ($this->checkImage($images) === true) {
                $results = $images;
            }
        }

        $composedImages = $this->composeImages($images);

        $betterImages = $this->getBetterImage($composedImages);

        return $betterImages;
    }

    /**
     * Compose images to an array with width as a key
     *
     * @param array $images
     * @return array
     */
    private function composeImages($images)
    {
        if (empty($images)) {
            return;
        }
        $result = [];
        foreach ($images as $typeImages) {
            if (!is_array($typeImages)) {
                continue;
            }
            foreach ($typeImages as $image) {
                $result[$image->width] = $image;
            }
        }


        return $result;
    }

    /**
     * Take as big image by width as possible
     *
     * @param array $images
     * @return array
     */
    private function getBetterImage($images)
    {
        if (empty($images)) {
            return;
        }

        $result = [];
        ksort($images);
        $feedImages = $this->socialFeedImageSizes;

        foreach ($feedImages as $feedImageName) {
            $image = array_pop($images);
            $result[$feedImageName] = $image->src;
        }
        return $result;
    }

    /**
     * Get images from link attachment
     *
     * @param VKPostAttachment $attachment
     * @return array
     */
    private function getLinkImages($attachment)
    {
        $results = [];
        if (empty($attachment->link->photo)) {
            return;
        }
        foreach ($attachment->link->photo->sizes as $size) {
            if (!empty($size->src)) {
                $results[$size->type] = $size;
                //$results = $this->replaceSizes($sizesArr);
            }
        }
        return $results;
    }

    /**
     * Get images from photo attachment
     *
     * @param VKPostAttachment $attachment
     * @return array
     */
    private function getPhotoImages($attachment)
    {
        $results = [];
        foreach ($attachment->photo->sizes as $size) {
            if (!empty($size->src)) {
                $results[$size->type] = $size;
                //$results = $this->replaceSizes($sizesArr);
            }
        }
        return $results;
    }

    /**
     * Get images from video attachment
     *
     * @param VKPostAttachment $attachment
     * @return array
     */
    private function getVideoImages($attachment)
    {
        $results = [];
        $results[self::VK_API_IMAGE_HIGH] = new \stdClass();
        $results[self::VK_API_IMAGE_MEDIUM] = new \stdClass();
        $results[self::VK_API_IMAGE_LOW] = new \stdClass();
        if (!empty($attachment->video->photo_640)) {
            $results[self::VK_API_IMAGE_HIGH]->src = $attachment->video->photo_640;
            $results[self::VK_API_IMAGE_HIGH]->width = 640;
        } elseif (!empty($attachment->video->photo_800)) {
            $results[self::VK_API_IMAGE_HIGH]->src = $attachment->video->photo_800;
            $results[self::VK_API_IMAGE_HIGH]->width = 800;
        }
        if (!empty($attachment->video->photo_320)) {
            $results[self::VK_API_IMAGE_MEDIUM]->src = $attachment->video->photo_320;
            $results[self::VK_API_IMAGE_MEDIUM]->width = 320;
        }
        if (!empty($attachment->video->photo_130)) {
            $results[self::VK_API_IMAGE_LOW]->src = $attachment->video->photo_130;
            $results[self::VK_API_IMAGE_LOW]->width = 130;
        }

        return $results;
    }

    /**
     * Get images from album attachment
     *
     * @param VKPostAttachment $attachment
     * @return array
     */
    private function getAlbumImages($attachment)
    {
        $results = [];
        foreach ($attachment->album->thumb->sizes as $size) {
            if (!empty($size->src)) {
                $results[$size->type] = $size;
                //$results = $this->replaceSizes($sizesArr);
            }
        }
        return $results;
    }

    /**
     * Convert vk wiki type links to html links
     *
     * @param string $description
     * @return string
     */
    private function convertVKLinks($description)
    {
        $patterns = [
            '/\[(id[0-9]+)\|(.+?)\]/',
            '/\[(club[0-9]+)\|(.+?)\]/',
            '/\[(topic-[0-9]+_[0-9]+)\|(.+?)\]/',
            '/\[(app[0-9]+)\|(.+?)\]/',
            '/\[(photo-[0-9]+_[0-9]+)\|(.+?)\]/',
            '/\[(video-[0-9]+_[0-9]+)\|(.+?)\]/',
            '/#(.+?)\s/'];
        $replacements = [
            /* '<a href="https://vk.com/\1" target="_blank">\2</a>',
              '<a href="https://vk.com/\1" target="_blank">\2</a>',
              '<a href="https://vk.com/\1" target="_blank">\2</a>',
              '<a href="https://vk.com/\1" target="_blank">\2</a>',
              '<a href="https://vk.com/\1" target="_blank">\2</a>',
              '<a href="https://vk.com/\1" target="_blank">\2</a>',
              '<a href="https://vk.com/feed?section=search&q=%23\1" target="_blank">#\1</a>' */
            '\2',
            '\2',
            '\2',
            '\2',
            '\2',
            '\2',
            '#\1'
        ];
        return preg_replace($patterns, $replacements, $description);
    }

    /**
     * Check if we have all needed image resolutions
     *
     * @param array $imageArr
     * @return boolean
     */
    private function checkImage($imageArr)
    {
        if (empty($imageArr[self::VK_API_IMAGE_LOW]) or
            empty($imageArr[self::VK_API_IMAGE_MEDIUM]) or
            empty($imageArr[self::VK_API_IMAGE_HIGH])
        ) {
            return false;
        }
        return true;
    }

    /**
     * Get videos of the post
     * @param VkPost $post
     * @return string url to player
     */
    private function getVideo($post)
    {
        if (empty($post->attachments)) {
            return;
        }
        foreach ($post->attachments as $attachment) {
            if ($attachment->type != 'video') {
                continue;
            }
            if (!empty($attachment->player)) {
                return $attachment->player;
            }
            return;
        }
    }

    /**
     * Get post from link wall
     *
     * @param string $link url of vk community or user
     * @param integer $limit limit posts
     * @return array of \yii\socialfeed\VkFeedPost
     */
    public function getPosts($link, $limit = 200)
    {
        if (!$this->checkLink($link)) {
            return [];
        }

        $user = $this->getUserFromLink($link);
        $offset = 0;
        $allPosts = [];
        do {
            $response = $this->fetchData('wall.get', [
                'owner_id' => $user,
                'photo_sizes' => 1,
                'count' => self::VK_API_MAX_POSTS,
                'offset' => $offset
            ]);
            $offset += self::VK_API_MAX_POSTS;
            $allPosts = array_merge($allPosts, $this->getPostsFromRaw($response, $user));
        } while (count($allPosts) <= $limit and ! empty($response->response->items));
        return array_slice($allPosts, 0, $limit);
    }

    /**
     * Make array of VkFeedPost objects from raw vk api response
     *
     * @param VKPosts $response object of raw vk api response
     * @param object $user vk user object
     * @return array of \yii\socialfeed\VkFeedPost
     */
    private function getPostsFromRaw($response, $user)
    {
        $result = [];
        foreach ($response->response->items as $post) {
            $postId = $post->id;
            if (!empty($post->copy_history)) {
                $post = array_shift($post->copy_history);
            }
            $result[] = new VkFeedPost(
                $this->getAuthor($post->owner_id),
                $post->date,
                $this->convertVKLinks($post->text),
                $this->getPhoto($post),
                $this->getVideo($post),
                'https://vk.com/wall'.$user.'_'.$postId
            );
        }
        return $result;
    }

    /**
     * Querying API for a data
     * @return Api
     */
    public function fetchData($method, $args)
    {
        if (!$this->guzzle) {
            $this->guzzle = new \GuzzleHttp\Client( ['verify' => false] );
        }
        $args['v'] = self::VK_API_VERSION;
        $data = $this->guzzle->post(self::URL_VK_API.$method, ['form_params' => $args])->getBody();
        return json_decode($data);
    }

    /**
     * Make array for author
     * @param VkUser $user
     * @return array
     */
    private function getAuthorForGroup($user)
    {
        $result = [];
        $result['name'] = $user->name;
        $result['id'] = $user->id;
        $result['link'] = 'https://vk.com/'.$user->screen_name;
        $result['image'] = $user->photo_200;
        return $result;
    }

    /**
     * Make array for author
     * @param VkUser $user
     * @return array
     */
    private function getAuthorForUser($user)
    {
        $result = [];
        $result['id'] = $user->id;
        $result['name'] = $user->first_name.' '.$user->last_name;
        $result['link'] = 'https://vk.com/id'.$user->id;
        return $result;
    }

    /**
     * Get author of the post
     *
     * @param integer $userId of vk user
     * @return array
     */
    private function getAuthor($userId)
    {
        if (strpos($userId, '-') !== false) {
            $user = $this->getGroupById($userId);
            $author = $this->getAuthorForGroup($user);
        } else {
            $user = $this->getUserById($userId);
            $author = $this->getAuthorForUser($user);
        }
        return $author;
    }

    /**
     * Check incoming link to be owned of our network
     *
     * @param string $link
     * @return boolean
     */
    private function checkLink($link)
    {
        if (mb_strpos($link, "//vk.com")) {
            return true;
        }
        if (mb_strpos($link, "//www.vk.com")) {
            return true;
        }
        if (mb_strpos($link, "//www.vkontakte.com")) {
            return true;
        }
        if (mb_strpos($link, "//vkontakte.com")) {
            return true;
        }

        return false;
    }

    /**
     * Call Vk api to get user by id
     *
     * @param integer $userId
     * @return VkUser
     */
    private function getUserById($userId)
    {
        if (empty($userId)) {
            return;
        }
        $response = $this->fetchData('users.get', ['user_ids' => $userId]);
        if (!empty($response->error)) {
            return;
        }
        return $response->response[0];
    }

    /**
     * Call Vk api to get group by id
     *
     * @param integer $userId
     * @return VkGroup
     */
    private function getGroupById($userId)
    {
        $response = $this->fetchData('groups.getById', ['group_ids' => str_replace('-', '', $userId)]);
        if (!empty($response->error)) {
            return;
        }
        return $response->response[0];
    }

    /**
     * Detect vk userId from incoming link
     *
     * @param string $link
     * @return integer
     */
    private function getUserFromLink($link)
    {
//https://vk.com/id12405176
//https://vk.com/club122561641
        $linkParts = explode('/', $link);
        $userStr = array_pop($linkParts);
        $userId = $this->getUserById($userStr);
        $groupPrefix = '';
        if (!$userId) {
            $userId = $this->getGroupById($userStr);
            if (!$userId) {
                echo "\nInvalid vk url";
                return;
            }
            $groupPrefix = '-';
        }
        return $groupPrefix.$userId->id;
    }
}
