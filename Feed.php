<?php
/**
 * Created by PhpStorm.
 * User: adalekin
 * Date: 28.07.15
 * Time: 15:18
 */
namespace yii\socialfeed;

use Yii;
use yii\base\InvalidConfigException;

class Feed {
    const FACTORY_INSTAGRAM = 'instagram';
    const FACTORY_FACEBOOK = 'facebook';
    const FACTORY_YOUTUBE = 'youtube';
    const FACTORY_VK = 'vk';

    public static function createFeed($factory, $defaultOptions=[]) {
        switch ($factory) {
            case self::FACTORY_INSTAGRAM:
                // FIXME: do not raise exception
                if (class_exists('\\yii\\socialfeed\\InstagramFeedFactory', true)) {
                    return new InstagramFeedFactory(self::_getFactoryOptions($factory, $defaultOptions));
                }
                break;
            case self::FACTORY_FACEBOOK:
                if (class_exists('\\yii\\socialfeed\\FacebookFeedFactory', true)) {
                    return new FacebookFeedFactory(self::_getFactoryOptions($factory, $defaultOptions));
                }
                break;
            case self::FACTORY_YOUTUBE:
                if (class_exists('\\yii\\socialfeed\\YouTubeFeedFactory', true)) {
                    return new YouTubeFeedFactory(self::_getFactoryOptions($factory, $defaultOptions));
                }
                break;
            case self::FACTORY_VK:
                if (class_exists('\\yii\\socialfeed\\VkFeedFactory', true)) {
                    return new VkFeedFactory(self::_getFactoryOptions($factory, $defaultOptions));
                }
                break;
            default:
                throw new InvalidConfigException("Unknown factory: $factory");
        }
    }

    protected static function _getFactoryOptions($factory, $defaultOptions) {
        $options = [];
        if (!empty(Yii::$app->params[$factory])) {
            $options = Yii::$app->params[$factory];
        }

        return array_merge($options, $defaultOptions);
    }
}