<?php

namespace yii\socialfeed;

use Google_Client;
use Google_Service_YouTube;

class YouTubeFeedPost extends AbstractFeedPost {

    public function __construct($raw) {
        $this->author = [
            "id" => $raw['snippet']['channelId'],
            "name" => $raw['snippet']['channelTitle'],
            "image" => null,
            "link" => "http://youtube.com/channel/" . $raw['snippet']['channelId']
        ];

        $this->link = "http://youtube.com/watch/?v=" . $raw['snippet']['resourceId']['videoId'];
        $this->video = $this->link;

        $this->image = [
            "low_resolution" => $raw['snippet']['thumbnails']['medium']['url'],
            "thumbnail" => $raw['snippet']['thumbnails']['default']['url'],
            "standard_resolution" => $raw['snippet']['thumbnails']['high']['url']
        ];

        $this->description = $raw['snippet']['description'];
        $this->date = strtotime($raw['snippet']['publishedAt']);
    }

    function getAuthor() {
        return $this->author;
    }

    function getLink() {
        return $this->link;
    }

    function getDescription() {
        return $this->description;
    }

    function getDate() {
        return $this->date;
    }

    function getNetwork() {
        return "youtube";
    }

    function getImage() {
        return $this->image;
    }

    function getVideo() {
        return $this->video;
    }
}


class YouTubeFeedFactory extends AbstractFeedFactory {

    public function __construct($params) {
        $this->developer_key = $params['developer_key'];

        $this->api = new Google_Client();
        $this->api->setDeveloperKey($this->developer_key);
    }

    function getPosts($link, $limit = 50) {
        $youtube = new Google_Service_YouTube($this->api);

        if (preg_match('/(?:youtube\.com|youtu\.be)\/channel\/([\S]+)/', $link, $match)) {
            $channel_id = $match[1];
            $channels_response = $youtube->channels->listChannels('contentDetails', array("id" => $channel_id));

            foreach ($channels_response['items'] as $channels_result) {
                switch ($channels_result['kind']) {
                    case 'youtube#channel':
                        $playlist = $channels_result['contentDetails']['relatedPlaylists']['uploads'];
                        break;
                }
            }
        }

        if (preg_match('/(?:youtube\.com|youtu\.be)\/playlist\?list=([\S]+)/', $link, $match)) {
            $playlist = $match[1];
        }

        if (!isset($playlist)) {
            return [];
        }

        $plyalistitems_response = $youtube->playlistItems->listPlaylistItems(
            'snippet', array("playlistId" => $playlist, "maxResults" => $limit));

        $result = [];
        foreach ($plyalistitems_response['items'] as $playlistItems_result) {
            $result[] = new YouTubeFeedPost($playlistItems_result);
        }

        return $result;
    }
};