<?php
/**
 * Created by PhpStorm.
 * User: adalekin
 * Date: 28.07.15
 * Time: 15:23
 */

namespace yii\socialfeed;

use Facebook;

class FacebookFeedPost extends AbstractFeedPost {

    public function __construct($raw, $user) {
        $this->author = [
            "id" => $user->getId(),
            "name" => $user->getName(),
            "image" => null,
            "link" => "http://www.facebook.com/profile.php?id=" . $user->getId()
        ];

        $id_parts = explode('_', $raw['id']);
        $this->link = 'http://www.facebook.com/' . $id_parts[0] .'/posts/' . $id_parts[1];

        $this->image = null;
        if (isset($raw['picture'])) {
            $this->image = [
                "low_resolution" => $raw['full_picture'],
                "thumbnail" => $raw['picture'],
                "standard_resolution" => $raw['full_picture']
            ];
        }

        $this->video = null;
        if ($raw['type'] == 'video') {
            $this->video = $raw['source'];
        }

        if (isset($raw['description'])) {
            $this->description = $raw['description'];
        } else {
            $this->description = '';
        }

        $this->date = $raw['created_time']->getTimestamp();
    }

    function getAuthor() {
        return $this->author;
    }

    function getLink() {
        return $this->link;
    }

    function getDescription() {
        return $this->description;
    }

    function getDate() {
        return $this->date;
    }

    function getNetwork() {
        return "facebook";
    }

    function getImage() {
        return $this->image;
    }

    function getVideo() {
        return $this->video;
    }
}

class FacebookFeedFactory extends AbstractFeedFactory
{
    public function __construct($params)
    {
        $this->app_id = $params['app_id'];
        $this->app_secret = $params['app_secret'];

        $this->api = new Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret
        ]);

        $token_url = "https://graph.facebook.com/oauth/access_token?" .
            "client_id=" . $this->app_id .
            "&client_secret=" . $this->app_secret .
            "&grant_type=client_credentials";
        $access_token = file_get_contents($token_url);
        $access_token = json_decode($access_token, true)["access_token"];

        $this->api->setDefaultAccessToken($access_token);
    }

    function getPosts($link, $limit = 200)
    {
        if (!$this->_checkLink($link)) {
            return [];
        }

        $user = $this->_getUserFromLink($link);

        $response = $this->api->get('/' . $user->getId() . '/posts?fields=id,created_time,caption,description,type,full_picture,picture,source,link');
        $feedEdge = $response->getGraphEdge();

        $result = [];

        do {
            foreach ($feedEdge as $status) {
                // if ($status->getProperty('description')) {
                    $result[] = new FacebookFeedPost($status->asArray(), $user);
                // }
            }

            // TODO: splice array to limit
            if (count($result) > $limit) {
                break;
            }
        } while($feedEdge = $this->api->next($feedEdge));

        return $result;

    }

    private function _checkLink($link)
    {
        if (mb_strpos($link, "//facebook.com")) {
            return true;
        }
        if (mb_strpos($link, "//www.facebook.com")) {
            return true;
        }
        if (mb_strpos($link, "//fb.com")) {
            return true;
        }

        return false;
    }

    private function _getUserFromLink($link)
    {
        $linkParts = explode('/', $link);
        $userId = array_pop($linkParts);

        if (mb_strpos($userId, "-")) {
            $userParts = explode('-', $userId);
            $userId = array_pop($userParts);
        }

        $response = $this->api->get($userId);
        return $response->getGraphUser();
    }
};
