<?php
/**
 * Created by PhpStorm.
 * User: adalekin
 * Date: 28.07.15
 * Time: 15:36
 */

namespace yii\socialfeed;

/*
 *   Post classes
 */
abstract class AbstractFeedPost {
    abstract function getAuthor();
    abstract function getLink();
    abstract function getDescription();
    abstract function getDate();
    abstract function getNetwork();
    abstract function getImage();
    abstract function getVideo();
}